<?php
require("header.php");
authorization();
?>
<h4>
<center>
<?php
	echo "Hello ".$_SESSION['username'];
?>
</center>
<?php
$sql = "SELECT * FROM users WHERE id = '$_REQUEST[id]'";
// print_r($sql);die();
$result = $conn->query($sql);

if ($result->num_rows > 0) {
	$row = $result->fetch_assoc();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container" style="width: 50%;margin-top: 100px">
	<form action="edit_action.php" method="POST">
		<input type="hidden" class="form-control" id="name" value="<?php echo $row['id']; ?>" name="id">
		<div class="form-group">
			<label for="name">Display Name</label>
			<input type="name" class="form-control" id="name" value="<?php echo $row['name']; ?>" name="name">
		</div>		
		<div class="form-group">
			<label for="age">Age</label>
			<input type="text" class="form-control" id="age" value="<?php echo $row['age']; ?>" name="age">
		</div>
		<div class="form-group">
		  <label for="city">City</label>
		    <select id="city" name="city" class="form-control">
		      <option value="mumbai" <?php echo trim($row['city']) == 'mumbai' ? 'selected' : '';?>>Mumbai</option>
		      <option value="raigad" <?php echo trim($row['city']) == 'raigad' ? 'selected' : '';?>>Raigad</option>
		      <option value="pune" <?php echo trim($row['city']) == 'pune' ? 'selected' : '';?>>Pune</option>
		      <option value="thane" <?php echo trim($row['city']) == 'thane' ? 'selected' : '';?>>Thane</option>
		    </select> 
		</div>
		<div class="form-group">
		  <label for="status">Status</label>
		    <select id="status" name="status" class="form-control">
		      <option value="active" <?php echo $row['status'] == 'active' ? 'selected' : ''; ?> >Active</option>
		      <option value="inactive" <?php echo $row['status'] == 'inactive' ? 'selected' : ''; ?> >Inactive</option>
		    </select>
		</div>
		<div>
			<button type="submit" class="btn btn-default" value="update">Update</button>&nbsp;&nbsp;&nbsp;&nbsp;<button type="reset" class="btn btn-default" value="clear">Clear</button>
		</div>
	</form>
</div>
</body>
</html>