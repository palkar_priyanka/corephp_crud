<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container" style="border: 1px solid black;margin-top: 200px;height: 250px;width: 500px;">
  <h4>
    <span style="color: red;">
      <?php
      if(isset($_REQUEST['msg']) && $_REQUEST['msg'] != ''){
        echo $_REQUEST['msg'];
      }
      ?>
    </span>
  </h4>
  <form action="login_action.php" method="POST" style="margin-top: 20px">
    <div class="form-group">
      <label for="Username">Username:</label>
      <input type="text" class="form-control" id="Username" placeholder="Enter Username" name="username">
    </div>
    <div class="form-group">
      <label for="password">Password:</label>
      <input type="password" class="form-control" id="password" placeholder="Enter password" name="password">
    </div>
    <div>
      <button type="submit" class="btn btn-default">Login</button>
    </div>
  </form>
</div>

</body>
</html>