<?php
require("header.php");
authorization();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container" style="width: 50%;margin-top: 100px">
	<form action="add_action.php" method="POST">
		<div class="form-group">
			<label for="Username">Username</label>
			<input type="text" class="form-control" id="Username" placeholder="Enter Username" name="username">
		</div>
		<div class="form-group">
			<label for="password">Password</label>
			<input type="password" class="form-control" id="password" placeholder="Enter password" name="password">
		</div>
		<div class="form-group">
			<label for="name">Display Name</label>
			<input type="name" class="form-control" id="name" placeholder="Enter name" name="name">
		</div>		
		<div class="form-group">
			<label for="age">Age</label>
			<input type="text" class="form-control" id="age" placeholder="Enter age" name="age">
		</div>
		<div class="form-group">
		  <label for="city">City</label>
		    <select id="city" name="city" class="form-control">
		      <option value="mumbai">Mumbai</option>
		      <option value="raigad">Raigad</option>
		      <option value="pune">Pune</option>
		      <option value="thane">Thane</option>
		    </select> 
		</div>
		<div>
			<button type="submit" class="btn btn-default" value="add">Add</button>&nbsp;&nbsp;&nbsp;&nbsp;<button type="reset" class="btn btn-default" value="clear">Clear</button>
		</div>
	</form>
</div>
</body>
</html>
