<?php
require("header.php");
authorization();

$filename = 'users.csv';

$sql = "SELECT * FROM users";
$result = $conn->query($sql);

// file creation 
$file = fopen($filename, "w");

if ($result->num_rows > 0) {
	while($row = $result->fetch_row()){
		fputcsv($file, $row);
	}
}
fclose();
// download
header("content-Description : File Transfer");
header("content-Disposition : attachment; filename=".$filename);
header("content-Type : application/csv; ");

readfile($filename);

//deleting file
unlink($filename);
exit();
?>