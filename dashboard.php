<?php
require("header.php");
authorization();
?>
<h4>
<center>
<?php
	echo "Hello ".$_SESSION['username'];
?>
&nbsp;&nbsp;
<a href="add_form.php">Add Record</a> |
<a href="logout.php">Logout</a>
</h4>
</center>

<?php
$sql = "SELECT * FROM users";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<div class="container" style="margin-top: 20px;height: 210px;width: 100%;">
<h4>
<span style="color: green;">
<?php
  if(isset($_REQUEST['msg']) && $_REQUEST['msg'] != ''){
    echo $_REQUEST['msg'];
  }
?>
</span>
</h4>
<!-- <center><h4><a href="exportcsv.php">Export CSV</a></h4></center> -->
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>Sr. No.</th>
				<th>Username</th>
				<th>Name</th>
				<th>Age</th>
				<th>City</th>
				<th>Status</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
<?php
    while($row = $result->fetch_assoc()){
    	// echo "<pre>";
    	// print_r($row);
    	// echo "</pre>";
  ?>
    	<tr>
	    	<td><?php echo $row['id']; ?></td>
	    	<td><?php echo $row['username']; ?></td>
	    	<td><?php echo $row['name']; ?></td>
	    	<td><?php echo $row['age']; ?></td>
	    	<td><?php echo $row['city']; ?></td>
	    	<td><?php echo $row['status']; ?></td>
	    	<td>
	    		<a href="edit_form.php?id=<?php echo $row['id']; ?>">Edit</a> | 
	    		<a href="delete.php?id=<?php echo $row['id']; ?>" onclick="return confirm('Are you sure you want to delete this record ?');">Delete</a> 
	    	</td>
    	</tr>
<?php
    }
?>
		</tbody>
	</table>
</div>
<?php
}else{
	echo "No Records Found";
}
?>