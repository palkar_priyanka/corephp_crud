<?php

function getConn(){

	$servername = "localhost";
	$username = "root";
	$password = "";
	$dbname = "piyadb";

// Create connection
	$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	}
	//echo "Connected successfully";
	return $conn;
}

function authorization(){
	session_start();

	if(!isset($_SESSION['username'])){
		header("Location:login_form.php");
	}
}

?>